import matplotlib.pyplot as plt
import numpy as np


# once
x0 = [26, 27, 28, 29, 30]
y0 = [10000, 9831, 5498, 859, 59]

#once fd
x1 = [34, 35, 36, 37, 38, 39, 40, 41, 42, 43]
y1 = [10000, 9851, 7676, 5387, 3151, 1121, 467, 93, 21, 3]

# flood
x2=[27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,43]
y2=[10000, 9997,9995,9985,9964,9868,9738,9285,8766,7665,6365,4681,2996,1637,895,30]

# flood fd
x3 = [41, 42, 46, 47, 48]
y3 = [10000, 9938, 9620, 5107, 1460]

x0.append(x0[-1])
x1.append(x1[-1])
x2.append(x2[-1])
x3.append(x3[-1])

y0.append(0)
y1.append(0)
y2.append(0)
y3.append(0)

plt.rc('font', size=20)
plt.plot(x0, y0, x1, y1, x2, y2, x3, y3, linewidth=5)

#plt.vlines(x = [120, 240, 360, 480], ymin =  sum(his0) - 1500, ymax = sum(his0), colors = 'black',
#           label = 'vline_single - full height')

plt.legend(["once", "once-fd", "flood", "flood-fd"], fontsize=18, loc="lower left")
plt.xlabel("Latency [us]", fontsize=20)
plt.ylabel("Message Count", fontsize=20)
plt.yscale('log')
#plt.xlim(0)
plt.title("RTEMS CAN Stack Read to Write Latency Cumulative Histogram", fontsize=25)
plt.show()


plt.show()
