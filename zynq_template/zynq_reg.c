/**
 * \file zynq_reg.c
 *
 * \author Michal Lenc, CTU FEE
 *
 * Initialize and map device driver base addresses
 */

#include <errno.h>
#include <rtems/error.h>
#include <rtems/mw_uid.h>
#include <rtems/rtems/intr.h>
#include <rtems/untar.h>
#include <semaphore.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "zynq_reg.h"

int zynq_peripheral_registers_map(zynq_registers_mapping_t *rrmap,
                                  int version) {
  if (rrmap->mapping_initialized) {
    return rrmap->mapping_initialized;
  }

  rrmap->mapping_initialized = -1;

  if (version == 0) {
    rrmap->fpga_map = (void *)(ZYNQ_FPGA0_OFFSET);
    rrmap->gpio_irq_pin = 0x04;
  } else {
    rrmap->fpga_map = (void *)(ZYNQ_FPGA1_OFFSET);
    rrmap->gpio_irq_pin = 0x40;
  }

  rrmap->fpga_base = (volatile void *)rrmap->fpga_map;

  rrmap->gpio_map = (void *)(ZYNQ_GPIO_OFFSET);

  rrmap->gpio_base = (volatile void *)rrmap->gpio_map;

  rrmap->mapping_initialized = 1;

  return rrmap->mapping_initialized;
}
