/*
 * zynq_apfoo.c
 *
 * Michal Lenc, CTU FEE
 *
 * Provides basic RTEMS app template.
 */

#include "app_def.h"
#include "system.h"
#include <errno.h>
#include <rtems/error.h>
#include <rtems/mw_uid.h>
#include <rtems/rtems/intr.h>
#include <rtems/untar.h>
#include <semaphore.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

int zynq_apfoo(int argc, char **argv) {
  printf("Hello world!\n");
  return 1;
}
