#ifndef _ZYNQ_REG_H
#define _ZYNQ_REG_H

typedef struct zynq_registers_mapping_t {
  int mapping_initialized;  /* true if registers were mapped */
  uint32_t gpio_irq_pin;    /* GPIO IRQ pin */
  void *fpga_map;           /* mapped FPGA registers */
  void *gpio_map;           /* mapped GPIO registers */
  volatile void *fpga_base; /* FPGA registers base address */
  volatile void *gpio_base; /* GPIO registers base address */
} zynq_registers_mapping_t;

#define ZYNQ_FPGA0_OFFSET 0x43c20000
#define ZYNQ_FPGA1_OFFSET 0x43c30000
#define ZYNQ_GPIO_OFFSET 0xe000a000

/* GPIO registers (TRM sect. 14 and B.19) */
#define GPIO_DATA_RO_OFFSET 0x068
#define GPIO_DIRM_OFFSET 0x284
#define GPIO_INT_EN_OFFSET 0x290
#define GPIO_INT_DIS_OFFSET 0x294
#define GPIO_INT_STATUS_OFFSET 0x298
#define GPIO_INT_TYPE_OFFSET 0x29c
#define GPIO_INT_POL_OFFSET 0x2a0
#define GPIO_INT_ANY_OFFSET 0x2a4

/* FPGA registers
 * (https://rtime.felk.cvut.cz/psr/cviceni/semestralka/#fpga-registers) */
#define FPGA_CR_OFFSET 0x0
#define FPGA_CR_PWM_EN 0x40

#define FPGA_SR_OFFSET 0x0004
#define FPGA_SR_IRC_A_MON 0x100
#define FPGA_SR_IRC_B_MON 0x200
#define FPGA_SR_IRC_IRQ_MON 0x400

#define FPGA_PWM_PERIOD_OFFSET 0x0008
#define FPGA_PWM_PERIOD_MASK 0x3fffffff

#define FPGA_PWM_DUTY_OFFSET 0x000c
#define FPGA_PWM_DUTY_MASK 0x3fffffff
#define FPGA_PWM_DUTY_DIR_A 0x40000000
#define FPGA_PWM_DUTY_DIR_B 0x80000000

#define FPGA_CR(rmap)                                                          \
  (*(volatile unsigned *)((rmap)->fpga_base + FPGA_CR_OFFSET))
#define FPGA_SR(rmap)                                                          \
  (*(volatile unsigned *)((rmap)->fpga_base + FPGA_SR_OFFSET))
#define FPGA_PWM_PERIOD(rmap)                                                  \
  (*(volatile unsigned *)((rmap)->fpga_base + FPGA_PWM_PERIOD_OFFSET))
#define FPGA_PWM_DUTY(rmap)                                                    \
  (*(volatile unsigned *)((rmap)->fpga_base + FPGA_PWM_DUTY_OFFSET))

#define GPIO_DIR(rmap)                                                         \
  (*(volatile unsigned *)((rmap)->gpio_base + GPIO_DIRM_OFFSET))
#define GPIO_INT_STAT(rmap)                                                    \
  (*(volatile unsigned *)((rmap)->gpio_base + GPIO_INT_STATUS_OFFSET))
#define GPIO_INT_EN(rmap)                                                      \
  (*(volatile unsigned *)((rmap)->gpio_base + GPIO_INT_EN_OFFSET))
#define GPIO_INT_DIS(rmap)                                                     \
  (*(volatile unsigned *)((rmap)->gpio_base + GPIO_INT_DIS_OFFSET))
#define GPIO_INT_TYPE(rmap)                                                    \
  (*(volatile unsigned *)((rmap)->gpio_base + GPIO_INT_TYPE_OFFSET))
#define GPIO_INT_POL(rmap)                                                     \
  (*(volatile unsigned *)((rmap)->gpio_base + GPIO_INT_POL_OFFSET))
#define GPIO_INT_ANY(rmap)                                                     \
  (*(volatile unsigned *)((rmap)->gpio_base + GPIO_INT_ANY_OFFSET))
#define GPIO_RAW(rmap)                                                         \
  (*(volatile unsigned *)((rmap)->gpio_base + GPIO_DATA_RO_OFFSET))

/* Functions definitions */

int zynq_peripheral_registers_map(
    zynq_registers_mapping_t *zynq_registers_mapping, int version);

#endif /* _ZYNQ_REG_H */
