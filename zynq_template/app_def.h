#ifndef _APP_DEF_H
#define _APP_DEF_H

#ifndef COND_EXTERN
#ifdef CONFIGURE_INIT
#define COND_EXTERN
#else
#define COND_EXTERN extern
#endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

#include "appl_config.h"

#ifdef CONFIG_OC_APP_ZYNQ_TEMPLATE_WITH_BSDNET
#define APPL_WITH_BSDNET
#endif

#define APP_VER_ID "zynq_template"

#include <semaphore.h>
#include <rtems/rtems/status.h>

void bad_rtems_status(rtems_status_code status, int fail_level,
                      const char *text);

static inline void check_rtems_status(rtems_status_code status, int fail_level,
                                      const char *text) {
  if (!rtems_is_status_successful(status))
    bad_rtems_status(status, fail_level, text);
}

int zynq_apfoo(int argc, char **argv);

#define SHELL_TASK_PRIORITY 50

#ifdef __cplusplus
}
#endif

#endif /*_APP_DEF_H*/
