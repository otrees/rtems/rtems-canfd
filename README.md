# RTEMS CAN Test application

This directory contains source code for new CAN/CAN FD stack for RTEMS.
Currently, the stack can be tested against two targets: virtual CAN and
CTU CAN FD soft core. Following sections describe compilation steps
required to build RTEMS application either on Xilinx-Zynq board or on
i386 architecture with virtualization over mainline x86-64 QEMU.

The subsystem is based on the concept of original LinCAN (Linux CAN) driver
which has been developed as the part of OrtCAN project.

The API to the driver is provided in the form of the POSIX character driver.
Each CAN controller (chip) is registered as node into "/dev" namespace by
name (i.e. "can0", "can1", ... ). It can be open multiple times and each
instance of the open can be accessed from the multiple threads.
Written messages are post into input end of queue (FIFO) which
is connected to the can controller (chip) or other frame processor on the
output side. Processor takes frames from FIFOs according their priority
class and transmit them to the network. When frame is successfully
sent it is echoed bask to all queues back to open file instances except
the sending one (that filtering is configurable) and these where given
frame is filtered out. Received frames a filtered to all queues to
applications ends of the queues which filter matches the CAN identifier
and frame type.

# RTEMS CAN Test Compilation

CAN Stack can be currently tested on Xilinx-Zynq MzAPO board or on i386
target using mainline x86-64 QEMU.

## i386 using QEMU

You can test our CAN stack using mainline QEMU qemu-system-x86_64 by compiling
the application for i386 target. Following steps are required to run
the application using QEMU.

```console
$ export PATH=$PATH:/opt/rtems/6/bin
$ cd targets/i386_pc686
$ ./setup-host-socketcan #might be required with sudo
$ ./qemu-i386-pc686-2x-ctu-pci-build
$ ./qemu-i386-pc686-2x-ctu-pci-run
```

It is expected that support for i386_pc686 is located in `/opt/rtems/6`.
You can use following script to compile BSP with required settings.

```console
$ ./i386-rtems-sys.cfg
```

Note that you might need to modify path to RTEMS source code directory
in RTEMS_DIR variable.

Both virtual CAN and CTU CAN FD targets can be tested on i386.

## Xilinx-Zynq MzAPO board
Compilation steps for Xilinx-Zynq:

```console
$ export PATH=$PATH:/opt/rtems/6/bin
$ make
```

It is expected that Xilinx BSP is build at `/opt/rtems`. You can use
following link https://gitlab.fel.cvut.cz/otrees/rtems/work-and-ideas/-/wikis/home
to compile RTEMS for Xilinx step by step or scripts located
in `targets/xilinx_zynq_mz_apo` directory,

# RTEMS CAN Test Commands

There are two test applications available: `can_1w` and `can_2w`. First application
sends messages from one device to other while the second one transfers messages
both ways. Devices have to be specified first.

Command `can_register -t [target]` registers supported CAN controller based
on argument `-t`. Argument `-h` can be used to list currently supported
targets. Controller is registered as a characteristic device on path
`dev/can0`, `dev/can1` etc.

Command `can_set_test_dev [dev0] [dev1]` set targets for CAN test applications.

## Running Tests for CTU CAN FD

Following steps can be used to run CAN applications for CTU CAN FD controller
target.

```console
# this registers two CTU CAN FD controllers under dev/can0 and dev/can1
SHLL [/] # can_register -t ctucanfd
# assign dev/can0 and dev/can1 to test applications
SHLL [/] # can_set_test_dev /dev/can0 /dev/can1
# run test applications
SHLL [/] # can_1w
SHLL [/] # can_2w
```

The same approach can be used for virtual controller.
