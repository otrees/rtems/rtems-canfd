/* SPDX-License-Identifier: BSD-2-Clause OR Apache-2.0 OR GPL-2.0-or-later */

/**
 * @file  can_latency_test.c
 *
 * @ingroup CAN
 *
 * @brief This file is part of RTEMS CAN test application
 */

/*
 * Copyright (C) 2024 Michal Lenc <michallenc@seznam.cz> FEE CTU
 * Copyright (C) 2024 Pavel Pisa <pisa@cmp.felk.cvut.cz> FEE CTU
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "app_def.h"
#include "system.h"
#include <errno.h>
#include <rtems/error.h>
#include <rtems/mw_uid.h>
#include <rtems/rtems/intr.h>
#include <rtems/untar.h>
#include <rtems/score/atomic.h>
#include <semaphore.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <inttypes.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>

#include <dev/can/can.h>
#include <dev/can/ctucanfd.h>

#include "can_test.h"
#include "can_register.h"

#define CAN_SEND_INTERVAL  10
#define CAN_SEND_BURST     4

#define CAN_HISTO_NUM      5000
#define CAN_HISTO_BURST    4

struct can_latency_test_priv {
  struct can_test_instance *can1;
  struct can_test_instance *can2;
  struct can_test_instance *can3;
  bool stop;
  int high_prio_sent;
  int high_prio_received;
  int low_prio_sent;
  int mid_prio_sent;
  uint64_t avg_latency;
  uint32_t histo[CAN_HISTO_BURST][CAN_HISTO_NUM];
};

rtems_task can_tester_monitor ( rtems_task_argument arg  )
{
  struct can_latency_test_priv *test = (struct can_latency_test_priv *)arg;
  struct can_test_instance *priv1 = test->can1;
  struct can_test_instance *priv2 = test->can2;
  struct can_test_instance *priv3 = test->can3;
  int received = -1;
  int sent = -1;
  int sent_low = -1;

  while ( test->stop == false ) {
    bool my_if = (received != test->high_prio_received) || (sent != test->high_prio_sent) ||
    (sent_low != test->low_prio_sent);
    if ( my_if ) {
      received = *(volatile int *)&test->high_prio_received;
      sent = *(volatile int *)&test->high_prio_sent;
      sent_low = *(volatile int *)&test->low_prio_sent;
      printf(
        "can_monitor: sent %d; received = %d; low sent %d; mid sent %d; high latency = %"PRId64" ns\n",
        received,
        sent,
        sent_low,
        test->mid_prio_sent,
        test->avg_latency
      );
    }

    struct timespec ts = {0, 10 * 1000000};
    clock_nanosleep( CLOCK_MONOTONIC, 0, &ts, NULL );
  }

  printf(
    "can_monitor final: sent %d; received = %d; low sent %d; mid sent %d; high latency = %"PRId64" ns\n",
    test->high_prio_sent,
    test->high_prio_received,
    test->low_prio_sent,
    test->mid_prio_sent,
    test->avg_latency
  );

  for (int j = 0; j < CAN_HISTO_BURST; j++) {
    printf("idx%d=[", j);
    for (int i = 0; i < CAN_HISTO_NUM; i++)
      {
        if (test->histo[j][i] != 0)
          {
            printf("%d,", i);
          }
      }
  printf("]\n");
  }
  for (int j = 0; j < CAN_HISTO_BURST; j++) {
    printf("his%d=[", j);
    for (int i = 0; i < CAN_HISTO_NUM; i++)
      {
        if (test->histo[j][i] != 0)
          {
            printf("%d,", test->histo[j][i]);
          }
      }
    printf("]\n");
  }

  struct timespec ts = {2, 10 * 1000000};
  clock_nanosleep( CLOCK_MONOTONIC, 0, &ts, NULL );

  Atomic_Uint active = _Atomic_Fetch_sub_uint(
    &priv1->active_tasks,
    1,
    ATOMIC_ORDER_SEQ_CST
    );
  if ( active == 1 ) {
    close( priv1->fd );
    printf( "%s: fd closed from monitor\n", priv1->devpath );
    free( priv1 );
  }

  active = _Atomic_Fetch_sub_uint(
    &priv2->active_tasks,
    1,
    ATOMIC_ORDER_SEQ_CST
    );
  printf("priv2 active = %d\n", active);
  if ( active == 1 ) {
    close( priv2->fd );
    printf( "%s: fd closed from monitor\n", priv2->devpath );
    free( priv2 );
  }

  active = _Atomic_Fetch_sub_uint(
    &priv3->active_tasks,
    1,
    ATOMIC_ORDER_SEQ_CST
    );
  printf("priv3 active = %d\n", active);
  if ( active == 1 ) {
    close( priv3->fd );
    printf( "%s: fd closed from monitor\n", priv3->devpath );
    free( priv3 );
  }

  rtems_task_exit();
}

rtems_task can1_receiver( rtems_task_argument arg )
{
  struct can_frame frame;
  struct can_latency_test_priv *test = (struct can_latency_test_priv *)arg;
  struct can_test_instance *priv = test->can1;
  int fd = priv->fd;
  int ret;
  int received = 0;
  unsigned int active;
  int eagain = 0;
  int idx;
  uint32_t send_timestamp = 0;
  uint32_t curr_timestamp = 0;
  uint32_t timestamp_diff;

  while ( true ) {
    ret = read( fd, &frame, sizeof( struct can_frame ) );
    if ( ret < 0 ) {
      if ( ret == -EAGAIN ) {
        if ( eagain == 0 ) {
          printf( "EAGAIN\n" );
          eagain = 1;
        }
        usleep( 100 );
        continue;
      }
      printf( "ERROR: read failed %d\n", ret );
      break;
    }
    eagain = 0;
    priv->received++;
    received += 1;
    if ( frame.header.can_id == 0x20) {
      test->high_prio_received += 1;
      if (test->high_prio_received == 10000) {
        test->stop = true;
      }
      curr_timestamp = (uint32_t)frame.header.timestamp;
      idx = frame.data[0] & 0xff;
      send_timestamp = ((uint64_t)frame.data[1] << 0);
      send_timestamp |= ((uint64_t)frame.data[2] << 8);
      send_timestamp |= ((uint64_t)frame.data[3] << 16);
      send_timestamp |= ((uint64_t)frame.data[4] << 24);

      if (idx >= CAN_HISTO_BURST) {
        continue;
      }
      timestamp_diff = curr_timestamp - send_timestamp;
      if (test->avg_latency == 0) {
        test->avg_latency = timestamp_diff;
      } else {
        test->avg_latency = (test->avg_latency + timestamp_diff) >> 1;
      }

      timestamp_diff = timestamp_diff / 100;
      if ((timestamp_diff < CAN_HISTO_BURST) || (idx < CAN_HISTO_NUM)) {
        test->histo[idx][timestamp_diff] += 1;
      }
    }
  }

  active = _Atomic_Fetch_sub_uint(
    &priv->active_tasks,
    1,
    ATOMIC_ORDER_SEQ_CST
  );
  if ( active == 1 ) {
    close( fd );
    printf( "%s: fd closed from read\n", priv->devpath );
    free( priv );
  }

  rtems_task_exit();
}

rtems_task can1_sender( rtems_task_argument arg ) {
  struct can_frame frame;
  struct can_latency_test_priv *test = (struct can_latency_test_priv *)arg;
  struct can_test_instance *priv = test->can1;
  int fd = priv->fd;
  unsigned int active;
  int ret;
  int i = 0;

  while ( test->stop == false ) {
    memset( &frame, 0, sizeof( struct can_frame ) );

    frame.header.can_id = 0x500;
    frame.header.dlen = 8;
    frame.data[0] = ( i >> 0 ) & 0xff;
    frame.data[1] = ( i >> 8 ) & 0xff;
    frame.data[2] = ( i >> 16 ) & 0xff;
    frame.data[3] = ( i >> 24 ) & 0xff;

    ret = write( fd, &frame, can_framesize( &frame ) );
    if ( ret != can_framesize( &frame ) ) {
      printf( "ERROR: write failed %d\n", ret );
      break;
    }
    priv->sent++;
    test->mid_prio_sent += 1;
    i += 1;
  }

  active = _Atomic_Fetch_sub_uint(
    &priv->active_tasks,
    1,
    ATOMIC_ORDER_SEQ_CST
    );
  if ( active == 1 ) {
    close( fd );
    printf( "%s: fd closed from write\n", priv->devpath );
    free( priv );
  }

  rtems_task_exit();
}

rtems_task can2_sender( rtems_task_argument arg ) {
  struct can_frame frame;
  struct can_latency_test_priv *test = (struct can_latency_test_priv *)arg;
  struct can_test_instance *priv = test->can2;
  int fd = priv->fd;
  unsigned int active;
  int ret;
  uint64_t i = 0;

  while ( test->stop == false ) {
    memset( &frame, 0, sizeof( struct can_frame ) );

    frame.header.can_id = 0x700;
    frame.header.dlen = 4;
    frame.data[0] = ( i >> 0 ) & 0xff;
    frame.data[1] = ( i >> 8 ) & 0xff;
    frame.data[2] = ( i >> 16 ) & 0xff;
    frame.data[3] = ( i >> 24 ) & 0xff;

    ret = write( fd, &frame, can_framesize( &frame ) );
    if ( ret != can_framesize( &frame ) ) {
      printf( "ERROR: write failed %d\n", ret );
      break;
    }

    test->low_prio_sent += 1;
    priv->sent++;
    i += 1;
  }

  active = _Atomic_Fetch_sub_uint(
    &priv->active_tasks,
    1,
    ATOMIC_ORDER_SEQ_CST
    );
  if ( active == 1 ) {
    close( fd );
    printf( "%d: fd closed from write\n", priv->sqn );
    free( priv );
  }

  rtems_task_exit();
}

/* High priority task sender */
rtems_task can3_sender( rtems_task_argument arg ) {
  struct can_frame frame;
  struct can_latency_test_priv *test = (struct can_latency_test_priv *)arg;
  struct can_test_instance *priv = test->can3;
  int fd = priv->fd;
  unsigned int active;
  int ret;
  uint64_t i = 0;
  uint64_t timestamp = 0;
  int burst_cnt = 0;

  while ( test->stop == false ) {
    memset( &frame, 0, sizeof( struct can_frame ) );

    ret = ioctl( priv->fd, RTEMS_CAN_CHIP_GET_TIMESTAMP, &timestamp );
    if (ret < 0) {
      printf("RTEMS_CAN_CHIP_GET_TIMESTAMP failed\n");
    } else {
      frame.data[0] = burst_cnt;
      frame.data[1] = ( timestamp >> 0 ) & 0xff;
      frame.data[2] = ( timestamp >> 8 ) & 0xff;
      frame.data[3] = ( timestamp >> 16 ) & 0xff;
      frame.data[4] = ( timestamp >> 24 ) & 0xff;
    }

    frame.header.can_id = 0x20;
    frame.header.dlen = 8;

    ret = write( fd, &frame, can_framesize( &frame ) );
    if ( ret != can_framesize( &frame ) ) {
      printf( "ERROR: write failed %d\n", ret );
      break;
    }

    test->high_prio_sent += 1;
    priv->sent++;
    i += 1;
    if ( ++burst_cnt >= priv->burst ) {
      struct timespec ts = {
        priv->send_interval / 1000,
        ( priv->send_interval % 1000 ) * 1000000
      };
      clock_nanosleep( CLOCK_MONOTONIC, 0, &ts, NULL );
      burst_cnt = 0;
    }
  }

  active = _Atomic_Fetch_sub_uint(
    &priv->active_tasks,
    1,
    ATOMIC_ORDER_SEQ_CST
    );
  if ( active == 1 ) {
    close( fd );
    printf( "%d: fd closed from write\n", priv->sqn );
    free( priv );
  }

  rtems_task_exit();
}

static void can_parse_args(
  int argc,
  char **argv,
  int *delay,
  int *burst
  )
{
  if ( argc >= 2 ) {
    *delay = strtol( argv[1], NULL, 0 );
  }

  if ( argc >= 3 ) {
    *burst = strtol( argv[2], NULL, 0 );
  }
}

static void can_fill_instance(
  struct can_test_instance *can1,
  struct can_test_instance *can2,
  struct can_test_instance *can3,
  int delay,
  int burst
)
{
  can1->devpath = "dev/can1";
  can3->devpath = can2->devpath = "dev/can2";

  can1->send_interval = can2->send_interval = can3->send_interval = delay;
  can1->burst = can2->burst = can3->burst = burst;

  _Atomic_Store_uint( &can1->active_tasks, 3, ATOMIC_ORDER_SEQ_CST );
  _Atomic_Store_uint( &can2->active_tasks, 2, ATOMIC_ORDER_SEQ_CST );
  _Atomic_Store_uint( &can3->active_tasks, 2, ATOMIC_ORDER_SEQ_CST );
}

int can_latency_test( int argc, char **argv )
{
  struct can_test_instance *can1;
  struct can_test_instance *can2;
  struct can_test_instance *can3;
  struct can_latency_test_priv *tester;
  rtems_id can1_id_tx, can1_id_rx, can2_id_tx, can3_id_tx, can_monitor;

  int ret;
  int delay = CAN_SEND_INTERVAL;
  int burst = CAN_SEND_BURST;

  can_parse_args( argc, argv, &delay, &burst );

  printf(
    "ctu_latency: params: delay %d, burst %d\n",
    delay,
    burst
    );

  tester = malloc( sizeof (struct can_latency_test_priv) );
  if ( tester == NULL ) {
    return -1;
  }

  can1 = malloc( sizeof( struct can_test_instance ) );
  if ( can1 == NULL ) {
    return -1;
  }

  can2 = malloc( sizeof( struct can_test_instance ) );
  if ( can2 == NULL ) {
    return -1;
  }

  can3 = malloc( sizeof( struct can_test_instance ) );
  if ( can3 == NULL ) {
    return -1;
  }

  memset( tester, 0, sizeof( struct can_latency_test_priv ) );
  memset( can1, 0, sizeof( struct can_test_instance ) );
  memset( can2, 0, sizeof( struct can_test_instance ) );
  memset( can3, 0, sizeof( struct can_test_instance ) );

  tester->can1 = can1;
  tester->can2 = can2;
  tester->can3 = can3;

  /* dev/can1: medium priority flood
   * dev/can2: low priority one thread, high priority other
   */

  can1->fd = open( can_test_dev1, O_RDWR );
  if ( can1->fd < 0 ) {
    printf( "ERROR: /dev/can1 open failed %d\n", can1->fd );
  }

  can2->fd = open( can_test_dev2, O_RDWR );
  if ( can2->fd < 0 ) {
    printf( "ERROR: dev/can2 open failed %d\n", can2->fd );
  }

  can3->fd = open( can_test_dev2, O_RDWR );
  if ( can3->fd < 0 ) {
    printf( "ERROR: dev/can2 open failed %d\n", can3->fd );
  }

  can_fill_instance( can1, can2, can3, delay, burst );

  /* Create priority queue for messages with 0x20 id */

  struct rtems_can_queue_param high_prio_queue;
  struct rtems_can_filter filter = {0x20, 0x7ff, 0, CAN_FRAME_ECHO};
  memset( &high_prio_queue, 0, sizeof( struct rtems_can_queue_param ) );
  high_prio_queue.direction = RTEMS_CAN_QUEUE_TX;
  high_prio_queue.priority = 1;
  high_prio_queue.buffer_size = 64;
  memcpy( &high_prio_queue.filter, &filter, sizeof( struct rtems_can_filter ) );

  ret = ioctl( can3->fd, RTEMS_CAN_CREATE_QUEUE, &high_prio_queue );
  if (ret < 0) {
    printf("RTEMS_CAN_CREATE_QUEUE failed %d\n", ret);
    return -1;
  }

  /* Start chips */

  ioctl( can1->fd, RTEMS_CAN_CHIP_START );
  ioctl( can2->fd, RTEMS_CAN_CHIP_START );

  rtems_task_create(
    rtems_build_name( 'C', 'A', 'N', 'T' ),
    122,
    RTEMS_MINIMUM_STACK_SIZE+0x1000,
    RTEMS_DEFAULT_MODES,
    RTEMS_DEFAULT_ATTRIBUTES,
    &can1_id_tx
  );

  rtems_task_create(
    rtems_build_name( 'C', 'A', 'N', 'T' ),
    122,
    RTEMS_MINIMUM_STACK_SIZE+0x1000,
    RTEMS_DEFAULT_MODES,
    RTEMS_DEFAULT_ATTRIBUTES,
    &can2_id_tx
  );

  rtems_task_create(
    rtems_build_name( 'C', 'A', 'N', 'T' ),
    122,
    RTEMS_MINIMUM_STACK_SIZE+0x1000,
    RTEMS_DEFAULT_MODES,
    RTEMS_DEFAULT_ATTRIBUTES,
    &can3_id_tx
  );

  rtems_task_create(
    rtems_build_name( 'C', 'A', 'N', 'R' ),
    120,
    RTEMS_MINIMUM_STACK_SIZE+0x1000,
    RTEMS_DEFAULT_MODES,
    RTEMS_DEFAULT_ATTRIBUTES,
    &can1_id_rx
  );

  rtems_task_create(
    rtems_build_name( 'C', 'A', 'N', 'M' ),
    200,
    RTEMS_MINIMUM_STACK_SIZE+0x1000,
    RTEMS_DEFAULT_MODES,
    RTEMS_DEFAULT_ATTRIBUTES,
    &can_monitor
  );

  rtems_task_start( can_monitor, can_tester_monitor, (rtems_task_argument)tester );
  rtems_task_start( can1_id_rx, can1_receiver, (rtems_task_argument)tester );
  rtems_task_start( can1_id_tx, can1_sender, (rtems_task_argument)tester );
  rtems_task_start( can2_id_tx, can2_sender, (rtems_task_argument)tester );
  rtems_task_start( can3_id_tx, can3_sender, (rtems_task_argument)tester );

  return 0;
}

