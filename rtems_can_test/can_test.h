/* SPDX-License-Identifier: BSD-2-Clause OR Apache-2.0 OR GPL-2.0-or-later */

/**
 * @file  can_test.h
 *
 * @ingroup CAN
 *
 * @brief This file is part of RTEMS CAN test application
 */

/*
 * Copyright (C) 2024 Michal Lenc <michallenc@seznam.cz> FEE CTU
 * Copyright (C) 2024 Pavel Pisa <pisa@cmp.felk.cvut.cz> FEE CTU
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _CAN_TEST_H
#define _CAN_TEST_H

extern char can_test_dev1[32];
extern char can_test_dev2[32];

struct can_test_instance {
  Atomic_Uint active_tasks;
  int fd;
  int frames_to_send;
  int frames_to_receive;
  int burst;
  int send_interval;
  int received;
  int sent;
  unsigned int sqn;
  struct timespec start;
  struct timespec end;
  char *devpath;
};

void can_start_rx_tasks(
  struct can_test_instance *can1,
  struct can_test_instance *can2
);
void can_start_tx_tasks(
  struct can_test_instance *can1,
  struct can_test_instance *can2
);
void can_start_monitor_tasks(
  struct can_test_instance *can1,
  struct can_test_instance *can2
);

#endif /* _CAN_TEST_H */
