/* SPDX-License-Identifier: BSD-2-Clause OR Apache-2.0 OR GPL-2.0-or-later */

/**
 * @file  can_register.c
 *
 * @ingroup CAN
 *
 * @brief This file is part of RTEMS CAN test application
 */

/*
 * Copyright (C) 2024 Michal Lenc <michallenc@seznam.cz> FEE CTU
 * Copyright (C) 2024 Pavel Pisa <pisa@cmp.felk.cvut.cz> FEE CTU
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "app_def.h"
#include "system.h"
#include <errno.h>
#include <rtems/error.h>
#include <rtems/mw_uid.h>
#include <rtems/rtems/intr.h>
#include <rtems/untar.h>
#include <rtems/score/atomic.h>
#include <semaphore.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>

#include "can_register.h"

static int can_target_type;
int can_registered_devices[64];
Atomic_Uint candev_sqn = 0;

static void can_register_help()
{
  printf(
    "\ncan_register - register CAN device(s)\n"
    "\nUsage:"
    "\n   -h   prints this text"
    "\n   -t <target> type of device to register"
    "\n"
    "\nSupported targets:"
    "\n   virtual      - virtual CAN"
    "\n   ctucanfd     - CTU CAN FD driver"
    "\n"
    "\nExample:"
    "\n   can_register -t virtual"
    "\n\n"
  );
}

static void can_register_parse_args( int argc, char **argv )
{
  int i;
  while( ( i = getopt( argc, argv, "t:h" ) ) != -1 ) {
    switch( i ) {
      case 'h':
        can_register_help();
        can_target_type = CAN_REGISTER_NONE;
        break;
      case 't':
        if ( strcmp( optarg, "virtual" ) == 0 ) {
          can_target_type = CAN_REGISTER_VIRTUAL;
        }
        else if ( strcmp(optarg, "ctucanfd" ) == 0 ) {
          can_target_type = CAN_REGISTER_CTU;
        }
        else {
          can_target_type = CAN_REGISTER_INVALID;
        }
        break;
      default:
        can_register_help();
        break;
    }
  }
  optind = 1;
}

int can_list_registered( int argc, char **argv )
{
  Atomic_Uint num_of_registered = _Atomic_Load_uint(
    &candev_sqn,
    ATOMIC_ORDER_SEQ_CST
  );

  for ( int i = 0; i < num_of_registered; i++ ) {
    printf("\n/dev/can%d   ", i);
    switch ( can_registered_devices[i] ) {
      case CAN_REGISTER_VIRTUAL:
        printf("virtual");
        break;
      case CAN_REGISTER_CTU:
        printf("ctucanfd");
        break;
      default:
        printf("invalid");
        break;
    }
  }
  printf("\n");

  return RTEMS_SUCCESSFUL;
}

int can_register( int argc, char **argv )
{
  int ret = RTEMS_SUCCESSFUL;

  can_target_type = CAN_REGISTER_INVALID;

  can_register_parse_args( argc, argv );

  switch( can_target_type ) {
    case CAN_REGISTER_VIRTUAL:
      ret = can_virtual_register();
      break;
    case CAN_REGISTER_CTU:
#ifndef BSP_WITH_PCI
      ret = can_ctucanfd_register();
#else
      ret = can_ctucanfd_pci_register();
#endif
      break;
    case CAN_REGISTER_NONE:
      break;
    case CAN_REGISTER_INVALID:
    default:
      printf( "can_register: invalid target!\n" );
      ret = -1;
      break;
  }

  if ( ret != RTEMS_SUCCESSFUL ) {
    printf("can_register: failed to register targets.\n");
  }

  return RTEMS_SUCCESSFUL;
}
