/* SPDX-License-Identifier: BSD-2-Clause OR Apache-2.0 OR GPL-2.0-or-later */

/**
 * @file  can_gateway.c
 *
 * @ingroup CAN
 *
 * @brief This file is part of RTEMS CAN test application
 */

/*
 * Copyright (C) 2024 Michal Lenc <michallenc@seznam.cz> FEE CTU
 * Copyright (C) 2024 Pavel Pisa <pisa@cmp.felk.cvut.cz> FEE CTU
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "app_def.h"
#include "system.h"
#include <errno.h>
#include <rtems/error.h>
#include <rtems/mw_uid.h>
#include <rtems/rtems/intr.h>
#include <rtems/untar.h>
#include <rtems/score/atomic.h>
#include <semaphore.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>

#include <dev/can/can.h>
#include <dev/can/ctucanfd.h>

#include "can_test.h"
#include "can_register.h"

#define RTEMS_CAN_GW_DELAY  0

struct can_gateway_s {
  int rx;
  int tx;
  int delay;
};

rtems_task rtems_can_gateway_worker( rtems_task_argument arg )
{
  struct can_gateway_s *gw = (struct can_gateway_s *)arg;
  struct can_frame frame;
  struct timespec ts = { gw->delay / 1000, ( gw->delay % 1000 ) * 1000000 };
  int ret;

  while ( true ) {
    ret = read( gw->rx, &frame, sizeof( struct can_frame ) );
    if ( ret < 0 ) {
      printf( "rtems_can_gateway_worker: read failed %d\n", ret );
      break;
    }

    clock_nanosleep( CLOCK_MONOTONIC, 0, &ts, NULL );

    ret = write( gw->tx, &frame, can_framesize( &frame ) );
    if ( ret != can_framesize( &frame ) ) {
      printf( "rtems_can_gateway_worker: write failed %d\n", ret );
      break;
    }
  }

  rtems_task_exit();
}

static void can_parse_args(
  int argc,
  char **argv,
  int *delay
)
{
  if ( argc >= 2 ) {
    *delay = strtol( argv[1], NULL, 0 );
  }
}

int rtems_can_gateway( int argc, char **argv )
{
  struct can_gateway_s *gw;
  rtems_id gw_task_id;

  int delay = RTEMS_CAN_GW_DELAY;

  can_parse_args( argc, argv, &delay );

  printf(
    "can_gateway: from %s to %s with delay %d\n",
    can_test_dev1,
    can_test_dev2,
    delay
  );

  gw = malloc( sizeof (struct can_gateway_s) );
  if ( gw == NULL ) {
    return -1;
  }

  memset( gw, 0, sizeof( struct can_gateway_s ) );

  gw->delay = delay;

  gw->tx = open( can_test_dev1, O_RDWR );
  if ( gw->tx < 0 ) {
    printf( "ERROR: %s open failed %d\n", can_test_dev1, gw->tx );
    return -1;
  }

  gw->rx = open( can_test_dev2, O_RDWR );
  if ( gw->rx < 0 ) {
    printf( "ERROR: %s open failed %d\n", can_test_dev2, gw->rx );
    return -1;
  }

  /* Start chips */

  ioctl( gw->rx, RTEMS_CAN_CHIP_START );
  ioctl( gw->tx, RTEMS_CAN_CHIP_START );

  rtems_task_create(
    rtems_build_name( 'C', 'A', 'N', 'G' ),
    120,
    RTEMS_MINIMUM_STACK_SIZE+0x1000,
    RTEMS_DEFAULT_MODES,
    RTEMS_DEFAULT_ATTRIBUTES,
    &gw_task_id
  );

  rtems_task_start(
    gw_task_id,
    rtems_can_gateway_worker,
    (rtems_task_argument)gw
  );

  return 0;
}
