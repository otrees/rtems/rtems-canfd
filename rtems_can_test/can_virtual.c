/* SPDX-License-Identifier: BSD-2-Clause OR Apache-2.0 OR GPL-2.0-or-later */

/**
 * @file  can_virtual.c
 *
 * @ingroup CAN
 *
 * @brief This file is part of RTEMS CAN test application
 */

/*
 * Copyright (C) 2024 Michal Lenc <michallenc@seznam.cz> FEE CTU
 * Copyright (C) 2024 Pavel Pisa <pisa@cmp.felk.cvut.cz> FEE CTU
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "app_def.h"
#include "system.h"
#include <errno.h>
#include <rtems/error.h>
#include <rtems/mw_uid.h>
#include <rtems/rtems/intr.h>
#include <rtems/untar.h>
#include <rtems/score/atomic.h>
#include <semaphore.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>

#include <dev/can/can-bus.h>
#include <dev/can/can-virtual.h>

#include "can_test.h"
#include "can_register.h"

static bool initialized = false;

int can_virtual_register( void ) {
  struct rtems_can_bus *bus;
  char devpath[32];
  int ret;

  if ( initialized ) {
    printf( "already initialize\n" );
    return RTEMS_SUCCESSFUL;
  }

  bus = malloc( sizeof( struct rtems_can_bus ) );

  bus->chip = rtems_can_virtual_initialize();
  if ( bus->chip == NULL ) {
    printf( "ERROR: virtual_initialize failed!\n" );
    return -1;
  }

  Atomic_Uint idx = _Atomic_Fetch_add_uint(
    &candev_sqn,
    1,
    ATOMIC_ORDER_SEQ_CST
  );

  sprintf( devpath, "dev/can%d", idx );

  ret = rtems_can_bus_register( bus, devpath );
  if ( ret != RTEMS_SUCCESSFUL ) {
    printf( "ERROR: can_bus_register failed\n" );
    return -1;
  }

  can_registered_devices[idx] = CAN_REGISTER_VIRTUAL;

  printf( "CAN virtual registered at %s\n", devpath );

  initialized = true;
  return RTEMS_SUCCESSFUL;
}
