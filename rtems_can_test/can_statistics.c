/* SPDX-License-Identifier: BSD-2-Clause OR Apache-2.0 OR GPL-2.0-or-later */

/**
 * @file  can_register.c
 *
 * @ingroup CAN
 *
 * @brief This file is part of RTEMS CAN test application
 */

/*
 * Copyright (C) 2024 Michal Lenc <michallenc@seznam.cz> FEE CTU
 * Copyright (C) 2024 Pavel Pisa <pisa@cmp.felk.cvut.cz> FEE CTU
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "app_def.h"
#include "system.h"
#include <errno.h>
#include <rtems/error.h>
#include <rtems/mw_uid.h>
#include <rtems/rtems/intr.h>
#include <rtems/untar.h>
#include <rtems/score/atomic.h>
#include <semaphore.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>

#include <dev/can/can.h>

static void can_statistics_help()
{
  printf(
    "\ncan_stats - view CAN statistics\n"
    "\nUsage:"
    "\n   can_stats [dev_path]"
    "\n"
    "\nExample:"
    "\n   can_register /dev/can0"
    "\n\n"
  );
}

static void can_statistics_display( char *path, struct rtems_can_stats *stats )
{
  char *chip_state;
  switch( stats->chip_state ) {
    case CAN_STATE_ERROR_ACTIVE:
      chip_state = "error active";
      break;
    case CAN_STATE_ERROR_WARNING:
      chip_state = "error warning";
      break;
    case CAN_STATE_ERROR_PASSIVE:
      chip_state = "error passive";
      break;
    case CAN_STATE_BUS_OFF:
      chip_state = "bus off";
      break;
    case CAN_STATE_STOPPED:
      chip_state = "stopped";
      break;
    case CAN_STATE_SLEEPING:
      chip_state = "sleeping";
      break;
    default:
      chip_state = "unknown";
      break;
  }

  printf(
    "\n"
    "%s:\n"
    "    RX packets %lu bytes %lu\n"
    "    RX errors  %lu overflows %lu\n"
    "    TX packets %lu bytes %lu\n"
    "    TX errors  %lu\n"
    "\n\n"
    "Chip state %s\n\n",
    path,
    stats->rx_done,
    stats->rx_bytes,
    stats->rx_error,
    stats->rx_overflows,
    stats->tx_done,
    stats->tx_bytes,
    stats->tx_error,
    chip_state
  );
}

int can_statistics( int argc, char **argv )
{
  struct rtems_can_stats stats;
  int fd;
  int ret;

  if ( argc != 2 ) {
    printf("ERROR: incorrect usage!\n");
    can_statistics_help();
    return -1;
  }

  fd = open( argv[1], O_RDWR );
  if ( fd < 0 ) {
    printf("ERROR: could not open %s: %d!\n", argv[1], fd);
    return -1;
  }

  ret = ioctl( fd, RTEMS_CAN_CHIP_STATISTICS, &stats );
  if ( ret < 0 ) {
    printf("ERROR: RTEMS_CAN_CHIP_STATISTICS failed: %d\n", ret);
    return -1;
  }

  close(fd);

  can_statistics_display( argv[1], &stats );

  return RTEMS_SUCCESSFUL;
}
