/* SPDX-License-Identifier: BSD-2-Clause OR Apache-2.0 OR GPL-2.0-or-later */

/**
 * @file  can_ctucanfd_pci.c
 *
 * @ingroup CAN
 *
 * @brief This file is part of RTEMS CAN test application
 */

/*
 * Copyright (C) 2024 Michal Lenc <michallenc@seznam.cz> FEE CTU
 * Copyright (C) 2024 Pavel Pisa <pisa@cmp.felk.cvut.cz> FEE CTU
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifdef BSP_WITH_PCI

#include "app_def.h"
#include "system.h"
#include <errno.h>
#include <rtems/error.h>
#include <rtems/mw_uid.h>
#include <rtems/rtems/intr.h>
#include <rtems/untar.h>
#include <rtems/score/atomic.h>
#include <semaphore.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>

#include <dev/can/can.h>
#include <dev/can/can-bus.h>
#include <dev/can/ctucanfd.h>

#include <rtems/pci.h>

#include "can_test.h"
#include "can_register.h"

static bool initialized = false;

static struct rtems_can_set_bittiming set_nominal_bittiming_1 = {
  .type = RTEMS_CAN_BITTIME_NOMINAL,
  .from = RTEMS_CAN_BITTIME_FROM_BITRATE,
  .bittiming = {
    .bitrate = 500000,
  }
};

static struct rtems_can_set_bittiming set_nominal_bittiming_2 = {
  .type = RTEMS_CAN_BITTIME_NOMINAL,
  .from = RTEMS_CAN_BITTIME_FROM_BITRATE,
  .bittiming = {
    .bitrate = 500000,
  }
};

static struct rtems_can_set_bittiming set_data_bittiming_1 = {
  .type = RTEMS_CAN_BITTIME_DATA,
  .from = RTEMS_CAN_BITTIME_FROM_BITRATE,
  .bittiming = {
    .bitrate = 2000000,
  }
};

static struct rtems_can_set_bittiming set_data_bittiming_2 = {
  .type = RTEMS_CAN_BITTIME_DATA,
  .from = RTEMS_CAN_BITTIME_FROM_BITRATE,
  .bittiming = {
    .bitrate = 2000000,
  }
};

int can_ctucanfd_pci_register( void )
{
  struct rtems_can_bus *bus1;
  struct rtems_can_bus *bus2;
  int bus;
  int dev;
  int fun;
  int status;
  int instance = 0;
  uint32_t base0;
  uint32_t base1;
  uint8_t irq;
  int fd1;
  int fd2;
  char devpath1[32];
  char devpath2[32];
  int ret;

  if ( initialized ) {
    printf( "CTU CAN FD over PCI already initialized\n" );
    return 0;
  }

  status = pci_find_device(
        0x1760, // for CTU CAN FD, 0x10e8 for Kvaser SJA1000
        0xff00, // for CTU CAN FD, 0x8406 for Kvaser SJA1000
        instance,
        &bus,
        &dev,
        &fun
      );

  if ( status != PCIB_ERR_SUCCESS ) {
    printf("ctucanfd_pci_init: pci_find_device failed: %d\n", status);
    return -1;
  }

  pci_read_config_dword( bus, dev, fun, PCI_BASE_ADDRESS_0, &base0 );
  pci_read_config_dword( bus, dev, fun, PCI_BASE_ADDRESS_1, &base1 );
  pci_read_config_byte( bus, dev, fun, PCI_INTERRUPT_LINE, &irq );

  bus1 = malloc( sizeof( struct rtems_can_bus ) );
  bus2 = malloc( sizeof( struct rtems_can_bus ) );

  *(volatile uint32_t *)( 0x43C10000 ) = 0x01000000;

  bus1->chip = rtems_ctucanfd_initialize(
    base1,
    irq,
    CTUCANFD_WORKER_PRIORITY,
    4,
    RTEMS_INTERRUPT_SHARED,
    100000000
  );
  if ( bus1->chip == NULL ) {
    printf( "ERROR: rtems_ctucanfd_initialize failed!\n" );
    return -1;
  }

  bus2->chip = rtems_ctucanfd_initialize(
    base1 + 0x4000,
    irq,
    CTUCANFD_WORKER_PRIORITY,
    4,
    RTEMS_INTERRUPT_SHARED,
    100000000
  );
  if ( bus2->chip == NULL ) {
    printf( "ERROR: rtems_ctucanfd_initialize failed!\n" );
    return -1;
  }

  Atomic_Uint idx = _Atomic_Fetch_add_uint(
    &candev_sqn,
    2,
    ATOMIC_ORDER_SEQ_CST
  );

  sprintf( devpath1, "dev/can%d", idx );
  sprintf( devpath2, "dev/can%d", idx + 1 );

  ret = rtems_can_bus_register( bus1, devpath1 );
  if ( ret != RTEMS_SUCCESSFUL ) {
    printf( "ERROR: rtems_can_bus_register failed\n" );
    return -1;
  }

  ret = rtems_can_bus_register( bus2, devpath2 );
  if ( ret != RTEMS_SUCCESSFUL ) {
    printf( "ERROR: rtems_can_bus_register failed\n" );
    return -1;
  }

  fd1 = open( devpath1, O_RDWR );
  fd2 = open( devpath2, O_RDWR );

  ioctl( fd1, RTEMS_CAN_SET_BITRATE, &set_nominal_bittiming_1);
  ioctl( fd1, RTEMS_CAN_SET_BITRATE, &set_data_bittiming_1);
  ioctl( fd2, RTEMS_CAN_SET_BITRATE, &set_nominal_bittiming_2);
  ioctl( fd2, RTEMS_CAN_SET_BITRATE, &set_data_bittiming_2);

  ioctl( fd1, RTEMS_CAN_CHIP_SET_MODE, CAN_CTRLMODE_FD );
  ioctl( fd2, RTEMS_CAN_CHIP_SET_MODE, CAN_CTRLMODE_FD );

  close( fd1 );
  close( fd2 );

  can_registered_devices[idx] = CAN_REGISTER_CTU;
  can_registered_devices[idx + 1] = CAN_REGISTER_CTU;

  printf( "CTU CAN FD over PCI registered at %s and %s\n", devpath1, devpath2 );
  return RTEMS_SUCCESSFUL;
}

#endif /* BSP_WITH_PCI */
