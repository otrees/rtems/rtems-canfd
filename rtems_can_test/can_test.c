/* SPDX-License-Identifier: BSD-2-Clause OR Apache-2.0 OR GPL-2.0-or-later */

/**
 * @file  can_test.c
 *
 * @ingroup CAN
 *
 * @brief This file is part of RTEMS CAN test application
 */

/*
 * Copyright (C) 2024 Michal Lenc <michallenc@seznam.cz> FEE CTU
 * Copyright (C) 2024 Pavel Pisa <pisa@cmp.felk.cvut.cz> FEE CTU
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "app_def.h"
#include "system.h"
#include <errno.h>
#include <rtems/error.h>
#include <rtems/mw_uid.h>
#include <rtems/rtems/intr.h>
#include <rtems/untar.h>
#include <rtems/score/atomic.h>
#include <semaphore.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>

#include <dev/can/can.h>
#include <dev/can/ctucanfd.h>

#include "can_test.h"

#define CAN_RX_TASKS_PRIO      120
#define CAN_TX_TASKS_PRIO      122
#define CAN_MONITOR_TASKS_PRIO 200

#define CAN_FRAMES_TO_SEND 2000
#define CAN_SEND_INTERVAL  1
#define CAN_SEND_BURST     100

char can_test_dev1[32];
char can_test_dev2[32];

Atomic_Uint cantest_sqn;

static int timespec_subtract(
  struct timespec *result,
  struct timespec *x,
  struct timespec *y
)
{
  /* Perform the carry for the later subtraction by updating Y. */
  if ( x->tv_nsec < y->tv_nsec ) {
    int num_sec = ( y->tv_nsec - x->tv_nsec ) / 1000000000 + 1;
    y->tv_nsec -= 1000000000 * num_sec;
    y->tv_sec += num_sec;
  }
  if ( x->tv_nsec - y->tv_nsec > 1000000000 ) {
    int num_sec = ( x->tv_nsec - y->tv_nsec ) / 1000000000;
    y->tv_nsec += 1000000000 * num_sec;
    y->tv_sec -= num_sec;
  }

  /* Compute the time remaining to wait.
     `tv_nsec' is certainly positive. */
  result->tv_sec = x->tv_sec - y->tv_sec;
  result->tv_nsec = x->tv_nsec - y->tv_nsec;

  /* Return 1 if result is negative. */
  return x->tv_sec < y->tv_sec;
}

rtems_task can_monitor( rtems_task_argument arg ) {
  struct can_test_instance *priv = ( struct can_test_instance * )arg;
  struct timespec res;
  int received = -1;
  int sent = -1;

  while ( _Atomic_Load_uint( &priv->active_tasks, ATOMIC_ORDER_SEQ_CST ) > 1 ) {
    if ( received != priv->received || sent != priv->sent ) {
      printf(
        "can_monitor: sqn %d: %s received = %d, sent = %d\n",
        priv->sqn,
        priv->devpath,
        priv->received,
        priv->sent
        );
      received = priv->received;
      sent = priv->sent;
    }

    struct timespec ts = {0, 10 * 1000000};
    clock_nanosleep( CLOCK_MONOTONIC, 0, &ts, NULL );
  }

  timespec_subtract( &res, &priv->end, &priv->start );

  printf(
    "can_monitor: sqn %d: dev = %s received = %d, sent = %d, took %lld ns\n",
    priv->sqn,
    priv->devpath,
    priv->received,
    priv->sent,
    ( ( res.tv_sec*1000000000LL + res.tv_nsec ) )
  );

  Atomic_Uint active = _Atomic_Fetch_sub_uint(
    &priv->active_tasks,
    1,
    ATOMIC_ORDER_SEQ_CST
    );
  if ( active == 1 ) {
    close( priv->fd );
    printf( "%d: fd closed from monitor\n", priv->sqn );
    free( priv );
  }

  rtems_task_exit();
}

rtems_task can_receiver( rtems_task_argument arg ) {
  struct can_frame frame;
  struct can_test_instance *priv = ( struct can_test_instance * )arg;
  int fd = priv->fd;
  int ret;
  int received = 0;
  unsigned int active;
  int eagain = 0;

  while ( priv->frames_to_receive != 0 ) {
    ret = read( fd, &frame, sizeof( struct can_frame ) );
    if ( ret < 0 ) {
      if ( ret == -EAGAIN ) {
        if ( eagain == 0 ) {
          printf( "EAGAIN\n" );
          eagain = 1;
        }
        usleep( 100 );
        continue;
      }
      printf( "ERROR: read failed %d\n", ret );
      break;
    }
    eagain = 0;
    priv->received++;
    received += 1;
    if ( received == priv->frames_to_receive ) {
      break;
    }
  }

  active = _Atomic_Fetch_sub_uint(
    &priv->active_tasks,
    1,
    ATOMIC_ORDER_SEQ_CST
  );
  if ( active <= 2 ) {
    /* Read timestamp */
    clock_gettime( CLOCK_MONOTONIC, &priv->end);
    if ( active == 1 ) {
      close( fd );
      printf( "%d: fd closed from read\n", priv->sqn );
      free( priv );
    }
  }
  rtems_task_exit();
}

rtems_task can_sender( rtems_task_argument arg ) {
  struct can_frame frame;
  struct can_test_instance *priv = (struct can_test_instance *)arg;
  int fd = priv->fd;
  unsigned int active;
  int ret;
  int i = 0;
  int burst_cnt = priv->burst;

  while ( i < priv->frames_to_send ) {
    memset( &frame, 0, sizeof( struct can_frame ) );

    frame.header.can_id = ( priv->sqn & 0xff ) | ( ( i % 7 ) << 8 ) ;
    frame.header.dlen = 4;
    frame.data[0] = ( i >> 0 ) & 0xff;
    frame.data[1] = ( i >> 8 ) & 0xff;
    frame.data[2] = ( i >> 16 ) & 0xff;
    frame.data[3] = ( i >> 24 ) & 0xff;

    ret = write( fd, &frame, can_framesize( &frame ) );
    if ( ret != can_framesize( &frame ) ) {
      printf( "ERROR: write failed %d\n", ret );
      break;
    }
    priv->sent++;
    i += 1;
    if ( --burst_cnt == 0 ) {
      struct timespec ts = {
        priv->send_interval / 1000,
        ( priv->send_interval % 1000 ) * 1000000
      };
      clock_nanosleep( CLOCK_MONOTONIC, 0, &ts, NULL );
      burst_cnt = priv->burst;
    }
  }

  if ( i != priv->frames_to_send ) {
    printf( "ERROR: %d writes done, %d required\n", i, priv->frames_to_send );
  }
  else {
    printf( "%d: write done\n", priv->sqn );
  }

  active = _Atomic_Fetch_sub_uint(
    &priv->active_tasks,
    1,
    ATOMIC_ORDER_SEQ_CST
    );

  if ( active <= 2 ) {
    /* Read timestamp */
    clock_gettime( CLOCK_MONOTONIC, &priv->end);
    if ( active == 1 ) {
      close( fd );
      printf( "%d: fd closed from write\n", priv->sqn );
      free( priv );
    }
  }

  rtems_task_exit();
}

void can_start_rx_tasks(
  struct can_test_instance *can1,
  struct can_test_instance *can2
  )
{
  rtems_id can1_id, can2_id;

  if ( can1 != NULL ) {
    rtems_task_create(
      rtems_build_name( 'C', 'A', 'N', 'R' ),
      CAN_RX_TASKS_PRIO,
      RTEMS_MINIMUM_STACK_SIZE+0x1000,
      RTEMS_DEFAULT_MODES,
      RTEMS_DEFAULT_ATTRIBUTES,
      &can1_id
      );

    rtems_task_start( can1_id, can_receiver, (rtems_task_argument)can1 );
  }

  if ( can2 != NULL ) {
    rtems_task_create(
      rtems_build_name( 'C', 'A', 'N', 'R' ),
      CAN_RX_TASKS_PRIO,
      RTEMS_MINIMUM_STACK_SIZE+0x1000,
      RTEMS_DEFAULT_MODES,
      RTEMS_DEFAULT_ATTRIBUTES,
      &can2_id
      );

    rtems_task_start( can2_id, can_receiver, (rtems_task_argument)can2 );
  }
}

void can_start_tx_tasks(
  struct can_test_instance *can1,
  struct can_test_instance *can2
  )
{
  rtems_id can1_id, can2_id;

  if ( can1 != NULL ) {
    rtems_task_create(
      rtems_build_name( 'C', 'A', 'N', 'T' ),
      CAN_TX_TASKS_PRIO,
      RTEMS_MINIMUM_STACK_SIZE+0x1000,
      RTEMS_DEFAULT_MODES,
      RTEMS_DEFAULT_ATTRIBUTES,
      &can1_id
      );

    rtems_task_start( can1_id, can_sender, (rtems_task_argument)can1 );
  }

  if ( can2 != NULL ) {
    rtems_task_create(
      rtems_build_name( 'C', 'A', 'N', 'T' ),
      CAN_TX_TASKS_PRIO,
      RTEMS_MINIMUM_STACK_SIZE+0x1000,
      RTEMS_DEFAULT_MODES,
      RTEMS_DEFAULT_ATTRIBUTES,
      &can2_id
      );

    rtems_task_start( can2_id, can_sender, (rtems_task_argument)can2 );
  }
}

void can_start_monitor_tasks(
  struct can_test_instance *can1,
  struct can_test_instance *can2
  )
{
  rtems_id can1_id, can2_id;

  if ( can1 != NULL ) {
    rtems_task_create(
      rtems_build_name( 'C', 'A', 'N', 'M' ),
      CAN_MONITOR_TASKS_PRIO,
      RTEMS_MINIMUM_STACK_SIZE+0x1000,
      RTEMS_DEFAULT_MODES,
      RTEMS_DEFAULT_ATTRIBUTES,
      &can1_id
      );

    rtems_task_start( can1_id, can_monitor, (rtems_task_argument)can1 );
  }

  if ( can2 != NULL ) {
    rtems_task_create(
      rtems_build_name( 'C', 'A', 'N', 'M' ),
      CAN_MONITOR_TASKS_PRIO,
      RTEMS_MINIMUM_STACK_SIZE+0x1000,
      RTEMS_DEFAULT_MODES,
      RTEMS_DEFAULT_ATTRIBUTES,
      &can2_id
      );

    rtems_task_start( can2_id, can_monitor, (rtems_task_argument)can2 );
  }
}

static void can_parse_args(
  int argc,
  char **argv,
  int *delay,
  int *count,
  int *burst
  )
{
  if ( argc >= 2 ) {
    *count = strtol( argv[1], NULL, 0 );
  }

  if ( argc >= 3 ) {
    *delay = strtol( argv[2], NULL, 0 );
  }

  if ( argc >= 4 ) {
    *burst = strtol( argv[3], NULL, 0 );
  }
}

static void can_fill_instance(
  struct can_test_instance *can1,
  struct can_test_instance *can2,
  int count,
  int delay,
  int burst,
  bool bothway
  )
{
  can1->sqn = _Atomic_Fetch_add_uint(
    &cantest_sqn,
    1,
    ATOMIC_ORDER_SEQ_CST
    );
  can2->sqn = _Atomic_Fetch_add_uint(
    &cantest_sqn,
    1,
    ATOMIC_ORDER_SEQ_CST
    );

  can1->devpath = can_test_dev1;
  can2->devpath = can_test_dev2;

  if ( bothway ) {
    can1->frames_to_send = can2->frames_to_send = count;
    can1->frames_to_receive = can2->frames_to_receive = count;
  }
  else {
    can1->frames_to_send = count;
    can2->frames_to_send = 0;
    can1->frames_to_receive = 0;
    can2->frames_to_receive = count;
  }

  can1->send_interval = can2->send_interval = delay;
  can1->burst = can2->burst = burst;

  _Atomic_Store_uint( &can1->active_tasks, 3, ATOMIC_ORDER_SEQ_CST );
  _Atomic_Store_uint( &can2->active_tasks, 3, ATOMIC_ORDER_SEQ_CST );
}

int can_test_2w( int argc, char **argv ) {
  struct can_test_instance *can1 = NULL;
  struct can_test_instance *can2 = NULL;

  int delay = CAN_SEND_INTERVAL;
  int count = CAN_FRAMES_TO_SEND;
  int burst = CAN_SEND_BURST;

  can_parse_args( argc, argv, &delay, &count, &burst );

  printf(
    "can_test_2w: params: count %d delay %d, burst %d\n",
    count,
    delay,
    burst
    );

  can1 = malloc( sizeof( struct can_test_instance ) );
  if ( can1 == NULL ) {
    return -1;
  }

  can2 = malloc( sizeof( struct can_test_instance ) );
  if ( can2 == NULL ) {
    return -1;
  }

  memset( can1, 0, sizeof( struct can_test_instance ) );
  memset( can2, 0, sizeof( struct can_test_instance ) );

  can1->fd = open( can_test_dev1, O_RDWR );
  if ( can1->fd < 0 ) {
    printf( "ERROR: %s open failed %d\n", can_test_dev1, can1->fd );
  }

  can2->fd = open( can_test_dev2, O_RDWR );
  if ( can2->fd < 0 ) {
    printf( "ERROR: %s open failed %d\n", can_test_dev2, can2->fd );
  }

  can_fill_instance( can1, can2, count, delay, burst, true );

  ioctl( can1->fd, RTEMS_CAN_CHIP_START );
  ioctl( can2->fd, RTEMS_CAN_CHIP_START );

  can_start_monitor_tasks( can1, can2 );
  can_start_rx_tasks( can1, can2 );
  can_start_tx_tasks( can1, can2 );

  return 0;
}

int can_test_1w( int argc, char **argv )
{
  struct can_test_instance *can1;
  struct can_test_instance *can2;

  int delay = CAN_SEND_INTERVAL;
  int count = CAN_FRAMES_TO_SEND;
  int burst = CAN_SEND_BURST;

  can_parse_args( argc, argv, &delay, &count, &burst );

  printf(
    "can_test_1w: params: count %d delay %d, burst %d\n",
    count,
    delay,
    burst
  );

  can1 = malloc( sizeof( struct can_test_instance ) );
  if ( can1 == NULL ) {
    return -1;
  }

  can2 = malloc( sizeof( struct can_test_instance ) );
  if ( can2 == NULL ) {
    return -1;
  }

  memset( can1, 0, sizeof( struct can_test_instance ) );
  memset( can2, 0, sizeof( struct can_test_instance ) );

  can1->fd = open( can_test_dev1, O_RDWR );
  if ( can1->fd < 0 ) {
    printf( "ERROR: %s open failed %d\n", can_test_dev1, can1->fd );
  }

  can2->fd = open( can_test_dev2, O_RDWR );
  if ( can2->fd < 0 ) {
    printf( "ERROR: %s open failed %d\n", can_test_dev2, can2->fd );
  }

  can_fill_instance( can1, can2, count, delay, burst, false );

  ioctl( can1->fd, RTEMS_CAN_CHIP_START );
  ioctl( can2->fd, RTEMS_CAN_CHIP_START );

  clock_gettime( CLOCK_MONOTONIC, &can1->start );
  clock_gettime( CLOCK_MONOTONIC, &can2->start );

  can_start_monitor_tasks( can1, can2 );
  can_start_rx_tasks( can1, can2 );
  can_start_tx_tasks( can1, can2 );

  return 0;
}

static void can_set_test_dev_parse_args( int argc, char **argv )
{
  int i;
  while( ( i = getopt( argc, argv, "h" ) ) != -1 ) {
    switch( i ) {
      case 'h':
      default:
        printf(
          "\ncan_set_test_dev - set CAN devices for test\n"
          "\nUsage: ncan_set_test_dev [dev1] [dev2]"
          "\n    - dev1: path to first CAN device"
          "\n    - dev2: path to second CAN device"
          "\n"
          "\nIn case of 1 way CAN test, dev1 will be used as"
          "\nsender and dev2 as receiver. Both will be sender and"
          "\nreceaver for 2 way test."
          "\nBoth dev1 and dev2 have to be specified even if they are"
          "\nthe same (as it is for virtual target)"
          "\nVirtual example:"
          "\n    can_set_test_dev dev/can0 dev/can0"
          "\n\n"
        );
        break;
    }
  }
  optind = 1;
}

int can_set_test_dev( int argc, char **argv )
{
  if ( argc != 3) {
    can_set_test_dev_parse_args( argc, argv );
    return 0;
  }

  memset(&can_test_dev1, '\0', 32);
  memset(&can_test_dev2, '\0', 32);

  memcpy(&can_test_dev1, argv[1], strlen(argv[1]));
  memcpy(&can_test_dev2, argv[2], strlen(argv[2]));

  printf(
    "Setting CAN paths for tests to %s and %s\n",
    can_test_dev1,
    can_test_dev2
  );

  return RTEMS_SUCCESSFUL;
}
