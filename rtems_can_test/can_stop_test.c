/* SPDX-License-Identifier: BSD-2-Clause OR Apache-2.0 OR GPL-2.0-or-later */

/**
 * @file  can_latency_test.c
 *
 * @ingroup CAN
 *
 * @brief This file is part of RTEMS CAN test application
 */

/*
 * Copyright (C) 2024 Michal Lenc <michallenc@seznam.cz> FEE CTU
 * Copyright (C) 2024 Pavel Pisa <pisa@cmp.felk.cvut.cz> FEE CTU
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "app_def.h"
#include "system.h"
#include <errno.h>
#include <rtems/error.h>
#include <rtems/mw_uid.h>
#include <rtems/rtems/intr.h>
#include <rtems/untar.h>
#include <rtems/score/atomic.h>
#include <semaphore.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <stdatomic.h>

#include <dev/can/can.h>
#include <dev/can/ctucanfd.h>

#include "can_test.h"
#include "can_register.h"

#define CAN_SEND_INTERVAL  10
#define CAN_SEND_BURST     4

rtems_task can1_task( rtems_task_argument arg ) {
  struct can_frame frame;
  struct can_test_instance *priv = (struct can_test_instance *)arg;
  int fd = priv->fd;
  unsigned int active;
  int ret;
  int i = 0;

  while ( priv->sent < 200 ) {
    memset( &frame, 0, sizeof( struct can_frame ) );

    frame.header.can_id = 0x500;
    frame.header.dlen = 8;
    frame.data[0] = ( i >> 0 ) & 0xff;
    frame.data[1] = ( i >> 8 ) & 0xff;
    frame.data[2] = ( i >> 16 ) & 0xff;
    frame.data[3] = ( i >> 24 ) & 0xff;

    ret = write( fd, &frame, can_framesize( &frame ) );
    if ( ret != can_framesize( &frame ) ) {
      printf( "ERROR: write failed %d\n", ret );
      break;
    }
    priv->sent++;
    i += 1;
  }

  struct timespec ts = {2, 0};
  ioctl( fd, RTEMS_CAN_CHIP_STOP, &ts );

  active = _Atomic_Fetch_sub_uint(
    &priv->active_tasks,
    1,
    ATOMIC_ORDER_SEQ_CST
    );
  if ( active == 1 ) {
    close( fd );
    printf( "%s: fd closed from write\n", priv->devpath );
    free( priv );
  }

  rtems_task_exit();
}

rtems_task can2_task( rtems_task_argument arg ) {
  struct can_frame frame;
  struct can_test_instance *priv = (struct can_test_instance *)arg;
  int fd = priv->fd;
  unsigned int active;
  int ret;
  volatile int i = 0;

  while ( i < 30 ) {
    memset( &frame, 0, sizeof( struct can_frame ) );

    frame.header.can_id = 0x200;
    frame.header.dlen = 4;
    frame.data[0] = ( i >> 0 ) & 0xff;
    frame.data[1] = ( i >> 8 ) & 0xff;
    frame.data[2] = ( i >> 16 ) & 0xff;
    frame.data[3] = ( i >> 24 ) & 0xff;

    ret = write( fd, &frame, can_framesize( &frame ) );
    if ( ret != can_framesize( &frame ) ) {
      printf( "ERROR: write failed %d\n", ret );
      break;
    }

    priv->sent++;
    i += 1;
  }

  struct timespec ts = {2, 0};
  clock_nanosleep( CLOCK_MONOTONIC, 0, &ts, NULL);
  ioctl( fd, RTEMS_CAN_CHIP_STOP, 0 );

  active = _Atomic_Fetch_sub_uint(
    &priv->active_tasks,
    1,
    ATOMIC_ORDER_SEQ_CST
    );
  if ( active == 1 ) {
    close( fd );
    printf( "%s: fd closed from write\n", priv->devpath );
    free( priv );
  }

  rtems_task_exit();
}

static void can_parse_args(
  int argc,
  char **argv,
  int *delay,
  int *burst
  )
{
  if ( argc >= 2 ) {
    *delay = strtol( argv[1], NULL, 0 );
  }

  if ( argc >= 3 ) {
    *burst = strtol( argv[2], NULL, 0 );
  }
}

static void can_fill_instance(
  struct can_test_instance *can1,
  struct can_test_instance *can2,
  int delay,
  int burst
)
{
  can1->devpath = "dev/can0";
  can2->devpath = "dev/can1";

  can1->send_interval = can2->send_interval = delay;
  can1->burst = can2->burst = burst;

  _Atomic_Store_uint( &can1->active_tasks, 1, ATOMIC_ORDER_SEQ_CST );
  _Atomic_Store_uint( &can2->active_tasks, 1, ATOMIC_ORDER_SEQ_CST );

}

int can_stop_test( int argc, char **argv )
{
  struct can_test_instance *can1;
  struct can_test_instance *can2;
  rtems_id can1_id_tx, can2_id_tx;

  int delay = CAN_SEND_INTERVAL;
  int burst = CAN_SEND_BURST;

  can_parse_args( argc, argv, &delay, &burst );

  printf(
    "can_stop_test: params: delay %d, burst %d\n",
    delay,
    burst
    );

  can1 = calloc( 1, sizeof( struct can_test_instance ) );
  if ( can1 == NULL ) {
    return -1;
  }

  can2 = calloc( 1, sizeof( struct can_test_instance ) );
  if ( can2 == NULL ) {
    return -1;
  }

  can1->fd = open( "/dev/can0", O_RDWR );
  if ( can1->fd < 0 ) {
    printf( "ERROR: /dev/can0 open failed %d\n", can1->fd );
  }

  can2->fd = open( "/dev/can1", O_RDWR );
  if ( can2->fd < 0 ) {
    printf( "ERROR: dev/can1 open failed %d\n", can2->fd );
  }

  can_fill_instance( can1, can2, delay, burst );

  /* Start chips */

  ioctl( can1->fd, RTEMS_CAN_CHIP_START );
  ioctl( can2->fd, RTEMS_CAN_CHIP_START );

  rtems_task_create(
    rtems_build_name( 'C', 'A', 'N', 'T' ),
    122,
    RTEMS_MINIMUM_STACK_SIZE+0x1000,
    RTEMS_DEFAULT_MODES,
    RTEMS_DEFAULT_ATTRIBUTES,
    &can1_id_tx
  );

  rtems_task_create(
    rtems_build_name( 'C', 'A', 'N', 'T' ),
    122,
    RTEMS_MINIMUM_STACK_SIZE+0x1000,
    RTEMS_DEFAULT_MODES,
    RTEMS_DEFAULT_ATTRIBUTES,
    &can2_id_tx
  );

  rtems_task_start( can1_id_tx, can1_task, (rtems_task_argument)can1 );
  rtems_task_start( can2_id_tx, can2_task, (rtems_task_argument)can2 );

  return 0;
}
